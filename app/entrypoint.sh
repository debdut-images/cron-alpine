#! /bin/ash

_start(){
    (
        exec cron-subshell
    ) &
    watch-dir &
    exec tail -f /var/log/cron.log "$@"
}

if test -z "$*"; then
    _start
elif [ "$1" = "logfile" ]; then
    _start $2
elif [ "$1" = "logfiles" ]; then
    _start $(cat $2)
else
    exec "$@"
fi
