from alpine:3.11

run apk add --no-cache perl
run mkdir /jobs /app /etc/periodic/always
run ash -c \
    'for x in /etc/periodic/*; do \
        ln -s $x /jobs/; \
    done'

env PATH="/app:${PATH}" WATCHDIR="/jobs"
copy --chown=0:0 app /app
copy --chown=0:0 __base /var/spool/cron/
run chmod +x /app/* && \
    touch /var/log/cron.log

workdir /jobs

entrypoint ["entrypoint.sh"]
